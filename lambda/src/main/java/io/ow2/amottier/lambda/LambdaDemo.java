package io.ow2.amottier.lambda;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class LambdaDemo {
    public static void main(String[] args) {
        Arrays.stream(args).sorted().forEach(System.out::println);

        List<Person> persons = new ArrayList<>();
        Person personA = new Person("Antoine", 37);
        Person personB = new Person("Élodie", 31);
        Person personC = new Person("aaa", 34);

        persons.add(personA);
        persons.add(personB);
        persons.add(personC);

        //persons.sort((a, b) -> Integer.compare(a.getAge(), b.getAge()));

        //persons.sort(Person::compare);

/*
        print(persons, (p) -> {
            return p.getAge() > 20;
        }, p -> {
            System.out.println(p);
        });

        printBis(persons, person -> {
            if(person.getAge() > personB.getAge()) {
                return 1;
            } else if (person.getAge() < personB.getAge()) {
                return -1;
            } else {
                return 0;
            }
        });

        invoke(() -> {
            System.out.println("aa");
            System.out.println("Test");
        });

        compute((a, b) -> a+b, 5, 6);

 */
    }

    private static void compute(OperationInterface operation, int a, int b) {
        System.out.println(operation.operation(a, b));
    }

    private static void print(List<Person> persons, Predicate<Person> filterInterface, Consumer<Person> consumer) {
        for(Person person : persons) {

            if(filterInterface.test(person)) {
                consumer.accept(person);
            }
        }
    }

    private static void printBis(List<Person> persons, Comparable<Person> compare) {
        for(Person person : persons) {
            if(compare.compareTo(person) == 0) {
                System.out.println(person);
            }
        }
    }

    private static void invoke(Runnable r) {
        r.run();
    }
}
