package io.ow2.amottier.lambda;

public interface OperationInterface {
    public int operation(int a, int b);
}
