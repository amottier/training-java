package io.ow2.amottier.lambda;

public class Person {
    private final String name;
    
    private final int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
    
    public static int compareTwo(Person a, Person b) {
        return Integer.compare(a.getAge(), b.getAge());
    }
    
    public int compare(Person person) {
        System.out.println("a");
        return Integer.compare(this.getAge(), person.getAge());
    }

    public Integer getAgeInteger(Person person) {
        return Integer.valueOf(person.getAge());
    }
}
