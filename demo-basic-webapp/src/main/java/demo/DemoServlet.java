package demo;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/demo")
public class DemoServlet extends HttpServlet {
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html");
        PrintWriter printWriter = resp.getWriter();
        printWriter.print("<html><body>");
        printWriter.print("<br />");

        printWriter.print("</body></html>");
        printWriter.close();
        
		super.doGet(req, resp);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 4549004081965704400L;

}
