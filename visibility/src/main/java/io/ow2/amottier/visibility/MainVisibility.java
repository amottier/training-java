package io.ow2.amottier.visibility;

import io.ow2.amottier.visibility.io.ow2.amottier.visibility.sub.PublicClassSubPackage;
//Fail: class private
//import io.ow2.amottier.visibility.io.ow2.amottier.visibility.sub.PrivateClassSubPackage;

public class MainVisibility {
    static int count;

    public static void main(String[] args) {
        PublicClassSubPackage publicClassSubPackage = new PublicClassSubPackage();
        publicClassSubPackage.publicBoolean = true;
    }

    protected static void protectedMethod() {
        System.out.println("protected");
    }

    static void defaultMethod() {
        System.out.println("default");
    }
}


