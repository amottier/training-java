package io.ow2.amottier.visibility.io.ow2.amottier.visibility.sub;

import io.ow2.amottier.visibility.MainVisibility;

public class PublicClassSubPackage {
    private int x;

    protected String text;

    float number;

    public boolean publicBoolean;

    public PublicClassSubPackage() {
        this(1);
        new MainVisibility();
        new PrivateClassSubPackage();
    }

    protected PublicClassSubPackage(int a) {
        this(a, "toto");
    }

    PublicClassSubPackage(int a, String text) {
        this(a, "toto", 3.14f);
    }

    private PublicClassSubPackage(int a, String text, float number) {
        x = a;
        this.text = text;
        this.number = number;
    }
}
