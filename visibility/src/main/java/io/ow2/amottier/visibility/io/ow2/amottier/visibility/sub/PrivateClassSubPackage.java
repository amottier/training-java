package io.ow2.amottier.visibility.io.ow2.amottier.visibility.sub;

class PrivateClassSubPackage {

    public PrivateClassSubPackage() {

    }

    public void publicPrint() {
        System.out.println("Hello public");
    }

    public void privatePrint() {
        System.out.println("Hello private");
    }

}
