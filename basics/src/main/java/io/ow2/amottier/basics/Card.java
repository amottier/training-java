package io.ow2.amottier.basics;

public class Card {

    private String rank;

    private String suit;

    public static String[] ranks = {"Ace", "King", "Queen", "Jack", "10", "9", "8", "7"};

    public static String[] suits = {"Clubs", "Spades", "Diamonds", "Heart"};

    public Card(String rank, String suit) {
        this.rank = rank;
        this.suit = suit;
    }

    public String toString() {
        return rank + " " + suit;
    }
}
