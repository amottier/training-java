package io.ow2.amottier.basics;

public class Deck {
    private Card[][] cards;

    public Deck() {
        cards = new Card[Card.suits.length][];
        for(int i = 0; i < Card.suits.length; i++) {
            String suit = Card.suits[i];
            cards[i] = new Card[Card.ranks.length];
            for(int j = 0; j < Card.ranks.length; j++) {
                String rank = Card.ranks[j];
                cards[i][j] = new Card(rank, suit);
                System.out.println(cards[i][j]);
            }
        }
    }
}
