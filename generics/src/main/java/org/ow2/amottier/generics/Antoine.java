package org.ow2.amottier.generics;

import org.ow2.amottier.generics.sub.SubAClassA;

import java.util.ArrayList;
import java.util.List;

public class Antoine<T> {

    T essai;

    public Antoine(T test) {
        essai = test;
    }

    public T getEssai() {
        return essai;
    }

    public static void truc(Antoine<Number> param) {
        param.getEssai();
    }

    public void test() {
        String[] a;
        a = new String[2];
        T[] b;
    }

    public static <T> T machin(T a, T b) {
        System.out.println(a.getClass());

        System.out.println(b.getClass());

        System.out.println(a.equals(b));

        return b;
    }


    public static void main(String[] args) {

        SubAClassA a = new SubAClassA();

        TopClass top = new TopClass();

        List<Antoine<? extends Number>> liste = new ArrayList<Antoine<? extends Number>>();
        Antoine a1 = new Antoine<Integer>(Integer.valueOf(3));
        Antoine a2 = new Antoine<Double>(Double.valueOf(3.5));
        liste.add(a1);
        liste.add(a2);


        //truc(antoine);

        Number a = machin(Double.valueOf(3.5), Integer.valueOf(3));
        System.out.println(a.getClass());

        //System.out.println(antoine.getEssai().getClass());
    }
}
