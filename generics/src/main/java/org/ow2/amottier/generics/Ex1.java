package org.ow2.amottier.generics;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class Ex1 {

    public static void main(String[] args) {
        Collection<Integer> collection = Arrays.asList(3, 5, 8, 9, 10, 14, 17, 18, 768);

        Object[] array = collection.toArray();
        System.out.println(Arrays.deepToString(array));
        swap(array, 2, 7);
        System.out.println(Arrays.deepToString(array));

        System.out.println(Ex1.count(collection, new OddCounter()));

        List<Integer> testInt = Arrays.asList(5, 1576, 54, 985, 8, 4, 1823, 76, 5, 1);
        System.out.println(maxElement(testInt, 3, 6));
    }

    public static <T> int count(Collection<T> collection, UnaryPredicate<T> unaryPredicate) {
        int count = 0;
        for(T element : collection) {
            if(unaryPredicate.keep(element)) {
                count++;
            }
        }
        return count;
    }

    public static <T> void swap(T[] array, int indexA, int indexB) {
        T elementA = array[indexA];
        array[indexA] = array[indexB];
        array[indexB] = elementA;
    }

    public static <T extends Object & Comparable<? super T>> T maxElement(List<? extends T> collection, int begin, int end) {
        T maxElement = collection.get(begin);
        for(int i = begin+1; i <= end; i++) {
            if(collection.get(i).compareTo(maxElement) > 0) {
                maxElement = collection.get(i);
            }
        }
        return maxElement;
    }

}
