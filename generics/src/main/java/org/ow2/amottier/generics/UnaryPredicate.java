package org.ow2.amottier.generics;

public abstract class UnaryPredicate<T> {
    public abstract boolean keep(T element);
}
