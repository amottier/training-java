package org.ow2.amottier.generics;

public class OddCounter extends UnaryPredicate<Integer> {


    @Override
    public boolean keep(Integer element) {
        return (element % 2 == 0);
    }
}
