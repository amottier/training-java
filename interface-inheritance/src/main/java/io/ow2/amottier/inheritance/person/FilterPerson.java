package io.ow2.amottier.inheritance.person;

public interface FilterPerson {
    public boolean keep(Person person);
}
