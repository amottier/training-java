public class Project {

	static int count;

	int contributorsNumber = 1;
	
	String projectName;
	
	Project() {
		this("default");
	}
	
	Project(String projectName) {
		this(projectName, 2);
	}
	
	Project(String projectName, int contributorsNumber) {
		this.projectName = projectName;
		this.contributorsNumber = contributorsNumber;
		contributorsNumber++;
		System.out.println("++"+count);
		count++;
		System.out.println("++done"+count);
	}
	
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	
	public String getProjectName() {
		return projectName;
		//return this.projectName;
	}
	
	public void setContributorsNumber(int contributorsNumber) {
		this.contributorsNumber = contributorsNumber;
	}
/*	
	public String toString() {
	   return "Object Project: " + contributorsNumber + " " + projectName + " " + count;
	}
	*/
	public static int incCount(int increment) {
		count += increment;
		return count;
	}

}
