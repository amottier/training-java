package io.ow2.amottier.inheritance.person;

public interface Person {
    public String getName();

    public int getAge();
}
