class ProjectOW2 extends Project {

	private String projectLeader;
/*	
	ProjectOW2(String projectLeader) {
		this("super-projet", projectLeader);
	}
*/
	ProjectOW2() {
		this("", "");
	}

	ProjectOW2(String projectName) {
		this(projectName, "toto");
	}
	
	ProjectOW2(String projectName, String projectLeader) {
		super(projectName);
		this.projectLeader = projectLeader;
	}

	
	public void setProjectLeader(String projectLeader) {
		this.projectLeader = projectLeader;
	}
	
	public String toString() {
	   return super.toString() + " " + this.projectLeader + " " + super.contributorsNumber;
	}
}
