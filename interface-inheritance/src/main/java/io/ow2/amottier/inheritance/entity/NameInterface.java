package io.ow2.amottier.inheritance.entity;

public interface NameInterface {
    public String getName();
}
