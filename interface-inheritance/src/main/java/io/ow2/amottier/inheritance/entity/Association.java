package io.ow2.amottier.inheritance.entity;

public class Association extends EntiteAsbtract implements AssociationInterface {

    public Association(String name) {
        super(name);
    }

    @Override
    public String getStatus() {
        return "Loi 1901";
    }

    @Override
    public String getName() {
        return "Association getName super.getName():" + super.getName();
    }
}
