package io.ow2.amottier.inheritance.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class MainEntity {

    public static void main(String[] args) {
        NameInterface ow2 = new Association("OW2");
        NameInterface eclipse = new Association("Eclipse");

        NameInterface mammut = new Entreprise("Mammut");
        Entreprise northFace = new Entreprise("North face");
        NameInterface northFaceNameInterface = northFace;

        perform(northFace);
        perform(northFaceNameInterface);

        List<NameInterface> objectsWithName = new ArrayList<>(4);
        objectsWithName.add(ow2);
        objectsWithName.add(eclipse);
        objectsWithName.add(mammut);
        objectsWithName.add(northFaceNameInterface);

        //objectsWithName.stream().forEach(e -> System.out.println(e.getName()));

        NameInterface[] objectsWithNameArray = {ow2, eclipse, mammut};

        /*
        NameInterface[] objectsWithNameArray = new NameInterface[4];

        objectsWithName.toArray(objectsWithNameArray);
*/
        for (NameInterface nameInterface : objectsWithNameArray) {
            System.out.println(nameInterface.getName());
        }
    }

    public static void perform(Entreprise entreprise) {
        System.out.println(entreprise.getClass().getName());
        System.out.println("Entreprise: "+ entreprise.getName());
    }

    public static void perform(NameInterface nameInterface) {
        System.out.println(nameInterface.getClass().getName());
        System.out.println("NameInterface: " + nameInterface.getName());
    }



}
