package io.ow2.amottier.inheritance.entity;

public class Entreprise extends EntrepriseAbstract implements NameInterface {

    public Entreprise(String name) {
        super(name, "123");
    }

    @Override
    public String getSiret() {
        return "1234";
    }

    @Override
    public String getName() {
        return "Entreprise getName super.getName():" + super.getName();
    }

}
