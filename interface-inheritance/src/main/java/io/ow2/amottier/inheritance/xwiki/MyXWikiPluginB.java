package io.ow2.amottier.inheritance.xwiki;

public class MyXWikiPluginB extends XwikiPluginAbstract {

    public MyXWikiPluginB(String name) {
        super(name);
    }

    @Override
    public void execute() {
        System.out.println("toto");
    }

    @Override
    public String getJSONRadarGraph(String projectName) {
        return null;
    }
}
