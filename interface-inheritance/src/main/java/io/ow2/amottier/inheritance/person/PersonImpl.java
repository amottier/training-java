package io.ow2.amottier.inheritance.person;

public class PersonImpl implements Person {
    private String name;
    private int age;

    PersonImpl(int age) {
        this.name = "name";
        this.age = age;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "PersonImpl{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
