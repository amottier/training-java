package io.ow2.amottier.inheritance.xwiki;

public class MyXWikiPluginA extends Object implements XWikiPluginInterface, PluginAuthor {

    private String name;

    public MyXWikiPluginA() {
        name = "toto";
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void execute() {
        System.out.println("Hello!");
    }

    @Override
    public String getJSONRadarGraph(String projectName) {
       return "";

    }

    public void demo() {

    }

    @Override
    public String getLastName() {
        return "Moi";
    }
}
