package io.ow2.amottier.inheritance.xwiki;

public interface XWikiPluginInterface {
    public String getName();

    public void execute();

    public String getJSONRadarGraph(String projectName);
}
