package io.ow2.amottier.inheritance.entity;

public abstract class EntiteAsbtract implements NameInterface {

    protected final String name;

    EntiteAsbtract(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return "EntiteAbstract getName() name: " + name;
    }
}
