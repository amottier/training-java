package io.ow2.amottier.inheritance;

import io.ow2.amottier.inheritance.entity.Association;
import io.ow2.amottier.inheritance.entity.AssociationInterface;
import io.ow2.amottier.inheritance.entity.Entreprise;
import io.ow2.amottier.inheritance.entity.NameInterface;
import io.ow2.amottier.inheritance.xwiki.MyXWikiPluginA;
import io.ow2.amottier.inheritance.xwiki.XWikiPluginInterface;

import java.util.ArrayList;
import java.util.List;

public class MyApp {

    public static void main(String[] args) {
        XWikiPluginInterface plugin = new MyXWikiPluginA();

        plugin.getName();

        String json = plugin.getJSONRadarGraph("ASM");

        List<NameInterface> trucs = new ArrayList<>();

        Association association = new Association("OW2");
        trucs.add(association);
        Entreprise entreprise = new Entreprise("Total");
        trucs.add(entreprise);

        for (NameInterface truc : trucs) {
            System.out.println(truc.getName());
        }

        List<AssociationInterface> assos = new ArrayList<>();
        assos.add(association);
        //assos.add(entreprise);

    }

}
