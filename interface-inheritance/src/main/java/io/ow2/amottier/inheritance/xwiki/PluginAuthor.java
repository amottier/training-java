package io.ow2.amottier.inheritance.xwiki;

public interface PluginAuthor {

    public String getLastName();

}
