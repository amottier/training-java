package io.ow2.amottier.inheritance.person;

import java.util.Arrays;
import java.util.stream.Collectors;

public class MainPerson {
    public static void main(String[] args) {

        Arrays.asList(4, 32, 25, 45, 6, 12, 178, 23, 18, 14).stream().map(PersonImpl::new).filter(p -> p.getAge() > 18).forEach(System.out::println);

    }

}
