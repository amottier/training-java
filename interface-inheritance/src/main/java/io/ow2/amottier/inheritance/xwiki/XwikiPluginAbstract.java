package io.ow2.amottier.inheritance.xwiki;

public abstract class XwikiPluginAbstract implements XWikiPluginInterface {

    private final String name;

    public XwikiPluginAbstract(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

}
