package io.ow2.amottier.inheritance.entity;

public abstract class EntrepriseAbstract extends EntiteAsbtract implements EntrepriseInterface {

    private String siret;

    EntrepriseAbstract(String name, String siret) {
        super(name);
        this.siret = siret;
    }

    @Override
    public String getSiret() {
        return siret;
    }
}
